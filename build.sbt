
import sbt._
import scala.sys.process._

//--------------------------------------------------

val scala_version = "2.12.3"
val project_name = "mauve_utils"
val project_version = "0.1.1"

//--------------------------------------------------

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x                             => MergeStrategy.first
}

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "mauve",
      scalaVersion := scala_version,
      version := project_version)),
    name := project_name,

    libraryDependencies += "org.scala-lang" % "scala-library" % scalaVersion.value,
    libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value,
    libraryDependencies += "org.scala-lang" % "scala-compiler" % scalaVersion.value,

    libraryDependencies += "com.esotericsoftware.yamlbeans" % "yamlbeans" % "1.13",
    libraryDependencies += "org.scalafx" %% "scalafx" % "8.0.144-R12")

//--------------------------------------------------

def release_lib = Command.command("release_lib") { state =>

  val cmd = s"""cp -f target/scala-2.12/${project_name}_2.12-${project_version}.jar release/${project_name}_lib.jar"""
  println(cmd)
  val x = cmd.!
  state
}

commands ++= Seq(release_lib)

//--------------------------------------------------
