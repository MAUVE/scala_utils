package mauve.utils

package object time {

  import scala.language.implicitConversions

  final case class Time(value: Long) {

    def +(t: Time): Time = Time(value + t.value)
    def -(t: Time): Time = Time(value - t.value)
    def *(l: Long): Time = Time(value * l)
    def /(l: Long): Time = Time(value / l)

    override def equals(that: Any): Boolean = that match {
      case Time(v) => value == v
      case _       => false
    }

    def >=(t: Time): Boolean = value >= t.value
    def <=(t: Time): Boolean = value <= t.value
    def >(t: Time): Boolean = value > t.value
    def <(t: Time): Boolean = value < t.value

    def min(t: Time): Time = Time(value min t.value)
    def max(t: Time): Time = Time(value max t.value)

    def sec: Double = (value: Double) / 1000000000
    def ms: Double = (value: Double) / 1000000
    def us: Double = (value: Double) / 1000
    def ns: Double = (value: Double)

    override def toString: String = {
      val ns = value % 1000
      val us = (value % 1000000) / 1000
      val ms = (value % 1000000000) / 1000000
      val sec = value / 1000000000
      (sec, ms, us, ns) match {
        case (0, 0, 0, 0) => "0"
        case (0, 0, 0, v) => s"""${v} ns"""
        case (0, 0, v, 0) => s"""${v} us"""
        case (0, v, 0, 0) => s"""${v} ms"""
        case (v, 0, 0, 0) => s"""${v} sec"""
        case (0, 0, x, y) => s"""${x} us ${y} ns"""
        case (0, x, y, 0) => s"""${x} ms ${y} us"""
        case (x, y, 0, 0) => s"""${x} sec ${y} ms"""
        case (0, x, y, z) => s"""${x} ms ${y} us ${z} ns"""
        case (x, y, z, 0) => s"""${x} sec ${y} ms ${z} us"""
        case _            => s"""${sec} sec ${ms} ms ${us} us ${ns} ns"""
      }
    }

    def pretty: String = {
      val ns = value % 1000
      val us = (value % 1000000) / 1000
      val ms = (value % 1000000000) / 1000000
      val sec = value / 1000000000
      val s = "%3d".format(sec)
      val m = "%3d".format(ms)
      val u = "%3d".format(us)
      val n = "%3d".format(ns)
      s"""${s} sec ${m} ms ${u} us ${n} ns"""
    }
  }

  final case class TimeConv(value: Long) {
    def sec: Time = Time(value * 1000000000)
    def ms: Time = Time(value * 1000000)
    def us: Time = Time(value * 1000)
    def ns: Time = Time(value)
  }

  implicit def time_to_Long(t: Time) = t.value
  implicit def long_to_TimeConv(t: Long) = TimeConv(t)

  def min(l: Iterable[Time]): Time = (l.head /: l.tail)(_ min _)
  def max(l: Iterable[Time]): Time = (l.head /: l.tail)(_ max _)
  def sum(l: Iterable[Time]): Time = (Time(0) /: l)(_ + _)
  def avg(l: Iterable[Time]): Time = sum(l) / l.size

}