package mauve.utils.yaml

import collection.JavaConverters._
import scala.reflect.runtime.universe._
import scala.collection.immutable.Map
import scala.language.dynamics

class Decoder(val obj: Any) extends Dynamic {

  def selectDynamic(key: String): Decoder = apply(key)

  def getType: Type = obj match {
    case _: String              => typeOf[String]
    case _: java.util.Map[_, _] => typeOf[java.util.Map[_, _]]
    case _: java.util.List[_]   => typeOf[java.util.List[_]]
    case _                      => typeOf[Object]
  }

  def map: Map[_, Decoder] = obj match {
    case map: java.util.Map[_, _] => map.asScala.toMap.map(x => (x._1, Decoder(x._2)))
    case _                        => throw new Exception(s"""object ${obj} is not a Map""")
  }

  def list: List[Decoder] = obj match {
    case map: java.util.List[_] => map.asScala.toList.map(Decoder(_))
    case _                      => throw new Exception(s"""object ${obj} is not a List""")
  }

  def keyType: Type =
    if (map.isEmpty) throw new Exception("map is empty")
    else Decoder(map.keys.head).getType

  def apply(key: String): Decoder = {
    val m = map
    if (keyType =:= typeOf[String]) Decoder(m.asInstanceOf[Map[String, _]](key))
    else throw new Exception("key type is not String")
  }

  def apply(index: Int): Decoder = Decoder(list(index))

  def asString: String = obj match {
    case x: String => x
    case _         => throw new Exception(s"""object ${obj} is not a String""")
  }

  def asInt: Int = asString.toInt

  def asDouble: Double = asString.toDouble

  override def toString = obj.toString
}

object Decoder {
  def apply(obj: Any): Decoder = obj match {
    case d: Decoder => d
    case _          => new Decoder(obj)
  }
}
