package mauve.utils.yaml

import java.io.FileReader
import com.esotericsoftware.yamlbeans.YamlReader

object Main extends App {
  val reader: YamlReader = new YamlReader(new FileReader("/home/ddoose/Projects/Mauve/git/mauve_pwcet/code/examples/example.yml"))
  val obj = reader.read()
  val sys = Decoder(obj)

  println(sys.scheduler)
  val tasks = sys.tasks
  for (task <- tasks.list) {
    println("name    : " + task.name)
    println("release : " + task.release)
    println("period  : " + task.period)
    println("deadline: " + task.deadline)
    for ((p, i) <- task.distribution.list.zipWithIndex) {
      println(s"""${i}: ${p.asDouble}""")
    }
  }
}
