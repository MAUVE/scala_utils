package mauve.utils.console

object TestColor extends App {
  def color(code: Int) = "\u001B[1m" + "\u001B[38;5;" + code + "m"
  // println(color(93) + "Mauve [2]: ")
  println(color(99) + "Mauve [2]: ")
  println(color(105) + "Mauve [2]: ")
  // println(color(111) + "Mauve [2]: ")
  println(color(147) + "Mauve [2]: ")
}

sealed abstract class Color {
  val ANSI_RESET = "\u001B[0m"
  val ANSI_BLACK = "\u001B[30m"
  val ANSI_RED = "\u001B[31m"
  val ANSI_GREEN = "\u001B[32m"
  val ANSI_YELLOW = "\u001B[33m"
  val ANSI_BLUE = "\u001B[34m"
  // val ANSI_PURPLE = "\u001B[35m"
  val ANSI_PURPLE = "\u001B[38;5;99m"
  val ANSI_CYAN = "\u001B[36m"
  val ANSI_WHITE = "\u001B[37m"

  def apply(string: String): String = this.toString + string + Default
}

case object Default extends Color {
  override def toString = ANSI_RESET
}

case object Black extends Color {
  override def toString = ANSI_BLACK
}
case object Red extends Color {
  override def toString = ANSI_RED
}
case object Green extends Color {
  override def toString = ANSI_GREEN
}
case object Yellow extends Color {
  override def toString = ANSI_YELLOW
}
case object Blue extends Color {
  override def toString = ANSI_BLUE
}
case object Purple extends Color {
  override def toString = ANSI_PURPLE
}
case object Cyan extends Color {
  override def toString = ANSI_CYAN
}
case object White extends Color {
  override def toString = ANSI_WHITE
}
