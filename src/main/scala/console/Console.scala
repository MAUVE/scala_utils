package mauve.utils.console

import scala.tools.nsc.interpreter._

abstract class ConsoleApp extends ILoop with App {

  //--------------------------------------------------

  import java.io._
  import scala.sys.process._
  import scala.tools.nsc.Settings
  import scala.reflect.internal.util.NoPosition
  import LoopCommand.{ cmd, nullary }

  //--------------------------------------------------

  def preprocess: Unit = {}

  def mauve(s: String): String = "\u001B[0m" + "\u001B[1m" + "\u001B[38;5;99m" + s + "\u001B[0m"

  def mauveLight(s: String): String = "\u001B[0m" + "\u001B[38;5;147m" + s + "\u001B[0m"

  private var count = 0
  override def prompt = {
    val res = if (count > 0) s"""Mauve [${count}]: """ else ""
    count += 1
    mauve(res)
  }

  override def printWelcome: Unit = {
    echo("\n" +
      "         \\,,,/\n" +
      "         (o o)\n" +
      "-----oOOo-(_)-oOOo-----")
  }

  //--------------------------------------------------

  protected val home: String = System.getProperty("user.dir")

  protected def withFolder[A](filename: String)(action: File => A): Option[A] = {
    val res = Some(new File(filename)) filter (f => f.exists && f.isDirectory) map action
    if (res.isEmpty) intp.reporter.warning(NoPosition, s"File `$filename' does not exist.")
    res
  }

  def pwdCommand: () => Result = () => {
    echo(System.getProperty("user.dir"))
    Result recording ":pwd"
  }

  def lsCommand(arg: String): Result = {
    def run(file: String, list: Boolean) = withFile(file) { f =>
      val str = if (list) s"""ls -l --color=always ${file}""" else s"""ls --color=always ${file}"""
      echo(str.!!)
      Result recording (if (list) s""":ls -l ${file}""" else s""":ls ${file}""")
    } getOrElse Result.default

    words(arg) match {
      case "-l" :: file :: Nil => run(file, true)
      case "-l" :: Nil         => run(System.getProperty("user.dir"), true)
      case file :: Nil         => run(file, false)
      case _                   => run(System.getProperty("user.dir"), false)
    }
  }

  def cdCommand(arg: String): Result = {
    def run(folder: String) = withFolder(folder) { f =>
      System.setProperty("user.dir", f.getAbsolutePath)
      Result recording s""":cd ${folder}"""
    } getOrElse Result.default

    words(arg) match {
      case folder :: Nil => run(folder)
      case _             => run(home)
    }
  }

  def userCommands: List[LoopCommand] = List()

  override lazy val standardCommands: List[LoopCommand] = userCommands ++ List(
    // Mauve Commands
    nullary("pwd", "pwd", pwdCommand),
    cmd("ls", "[-l] <folder>", "ls <folder>", lsCommand, fileCompletion),
    cmd("cd", "<folder>", "cd <folder>", cdCommand, fileCompletion),
     // ILoop Commands
     cmd("edit", "<id>|<line>", "edit history", editCommand),
     cmd("help", "[command]", "print this summary or command-specific help", helpCommand),
     historyCommand,
     cmd("h?", "<string>", "search the history", searchHistory),
     // cmd("imports", "[name name ...]", "show import history, identifying sources of names", importsCommand),
     // cmd("implicits", "[-v]", "show the implicits in scope", intp.implicitsCommand),
     // cmd("javap", "<path|class>", "disassemble a file or class name", javapCommand),
     cmd("line", "<id>|<line>", "place line(s) at the end of history", lineCommand),
     cmd("load", "<path>", "interpret lines in a file", loadCommand, fileCompletion),
     cmd("paste", "[-raw] [path]", "enter paste mode or paste a file", pasteCommand, fileCompletion),
     // nullary("power", "enable power user mode", powerCmd),
     nullary("quit", "exit the interpreter", () => Result(keepRunning = false, None)),
     cmd("replay", "[options]", "reset the repl and replay all previous commands", replayCommand, settingsCompletion),
     // cmd("require", "<path>", "add a jar to the classpath", require),
     cmd("reset", "[options]", "reset the repl to its initial state, forgetting all session entries", resetCommand, settingsCompletion),
     cmd("save", "<path>", "save replayable session to a file", saveCommand, fileCompletion),
     shCommand,
     // cmd("settings", "<options>", "update compiler options, if possible; see reset", changeSettings, settingsCompletion),
     // nullary("silent", "disable/enable automatic printing of results", verbosity),
     // cmd("type", "[-v] <expr>", "display the type of an expression without evaluating it", typeCommand),
     // cmdWithHelp("kind", kindUsage, "display the kind of a type. see also :help kind", Some(kindCommandDetailedHelp), kindCommand),
     // nullary("warnings", "show the suppressed warnings from the most recent line which had any", warningsCommand)
  )

  //--------------------------------------------------

  final override def createInterpreter(): Unit = {
    super.createInterpreter()

    echo(mauveLight("-------------------- loading --------------------"))
    preprocess
    intp.beQuietDuring {
      intp.bind("?", "Mauve Console")
    }
    echo(mauveLight("--------------------  ready  --------------------"))
  }

  def start: Unit = {
    val settings = new Settings
    settings.usejavacp.value = true
    settings.deprecation.value = true
    settings.processArgumentString("-feature -language:implicitConversions -language:dynamics -language:postfixOps") // -Xfatal-warnings  -deprecation -Xlint 
    process(settings)
  }
}
