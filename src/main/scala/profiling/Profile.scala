package mauv.utils.profiling

import java.lang.management._
import scala.collection.mutable.Map
import scala.collection.mutable.HashMap

object Profile {
  var debug: Boolean = false
  var _duration: Long = 0
  val status: Map[String, Entry] = new HashMap

  def duration: Long = _duration

  def start: Unit = {
    if (debug) println("Profile.start") else {}
    val bean: ThreadMXBean = ManagementFactory.getThreadMXBean
    val t: Long = bean.getCurrentThreadUserTime
    _duration = t
  }

  def stop: Unit = {
    if (debug) println("Profile.stop") else {}
    val bean: ThreadMXBean = ManagementFactory.getThreadMXBean
    val t: Long = bean.getCurrentThreadUserTime
    _duration += t - _duration
  }

  def start(s: String) = {
    val opt: Option[Entry] = status.get(s)
    if (opt.isDefined) {
      if (debug) println("Profile.start " + s + " (" + opt.get.iterations + ")") else {}
      opt.get.start
    } else {
      if (debug) println("Profile.start " + s) else {}
      val e: Entry = new Entry(s)
      status += s -> e
      e.start
    }
  }

  def stop(s: String) = {
    if (debug) println("Profile.stop  " + s) else {}
    val opt: Option[Entry] = status.get(s)
    if (opt.isDefined) opt.get.stop
  }

  def total: Long = {
    var tot: Long = 0
    status.values.foreach(tot += _.duration)
    tot
  }

  def iterations: Int = {
    var tot: Int = 0
    status.values.foreach(tot += _.iterations)
    tot
  }

  override def toString: String = {
    var s: String = ""
    val l: List[Entry] = status.values.toList.sortWith((e1, e2) => e1.duration <= e2.duration)
    l.foreach(e => s += String.format("%1$20s", e.name) + " : " + timeToString(e.duration) + " | " + e.iterations + "\n")
    s += "\n" + String.format("%1$20s", "Total") + " : " + timeToString(total) + " | " + iterations + "\n"
    s += String.format("%1$20s", "Global") + " : " + timeToString(duration) + "\n"
    s
  }

  def nanoTomsm(t: Long): (Long, Long, Long) = {
    val sec: Long = t / 1000000000
    val ms: Long = t % (1000000000) / 1000000
    (sec / 60, sec % 60, ms)
  }

  def timeToString(t: Long): String = {
    val min: java.lang.Long = t / 1000000000 / 60
    val sec: java.lang.Long = t / 1000000000 % 60
    val ms: java.lang.Long = t % (1000000000) / 1000000
    String.format("%1$5d min %2$2d sec %3$3d ms", min, sec, ms)
  }
}

class Entry(val name: String) {
  private var _iterations: Int = 0
  private var _duration: Long = 0
  private var _time: Long = 0

  def iterations: Int = _iterations
  def duration: Long = _duration

  def start: Unit = {
    val bean: ThreadMXBean = ManagementFactory.getThreadMXBean
    val t: Long = bean.getCurrentThreadUserTime
    _time = t
  }

  def stop: Unit = {
    val bean: ThreadMXBean = ManagementFactory.getThreadMXBean
    val t: Long = bean.getCurrentThreadUserTime
    _duration += t - _time
    _iterations += 1
  }

  override def toString: String = name + " : " + Profile.timeToString(duration) + " (" + iterations + ")"
}