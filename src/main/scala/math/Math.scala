package mauve.utils

package object math {
  def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

  def sum(l: Iterable[Double]): Double = ((0: Double) /: l)(_ + _)
  def avg(l: Iterable[Double]): Double = sum(l) / l.size.toDouble
}