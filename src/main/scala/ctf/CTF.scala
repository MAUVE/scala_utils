package mauve.utils.ctf

import org.eclipse.tracecompass.ctf.core.event.IEventDefinition
import org.eclipse.tracecompass.ctf.core.trace.CTFTrace
import org.eclipse.tracecompass.ctf.core.trace.CTFTraceReader
import scala.collection.mutable.ArrayBuffer

package object CTF {

  def load(fileName: String, maxSize: Int = -1): Seq[IEventDefinition] = {

    val CTF_trace = new CTFTrace(fileName)
    val reader = new CTFTraceReader(CTF_trace)
    val buffer = new ArrayBuffer[IEventDefinition]

    while (reader.hasMoreEvents && (maxSize == -1 || buffer.size <= maxSize)) {
      val event = reader.getCurrentEventDef
      buffer += event
      reader.advance
    }
    reader.close

    buffer.toSeq
  }

}