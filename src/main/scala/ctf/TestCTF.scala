package mauve.utils.ctf

import scala.collection.JavaConversions._

import org.eclipse.tracecompass.ctf.core.trace.CTFTrace
import org.eclipse.tracecompass.ctf.core.trace.CTFTraceReader
import org.eclipse.tracecompass.ctf.core.event.types.Definition
import org.eclipse.tracecompass.ctf.core.event.types.StringDefinition
import org.eclipse.tracecompass.ctf.core.event.types.IntegerDefinition
import org.eclipse.tracecompass.ctf.core.event.types.FloatDefinition

object CTF_Main extends App {
  val size = 100
  val CTF_trace = new CTFTrace("/home/ddoose/save/lttng-traces/auto-20171117-153140/ust/uid/1000/64-bit")
  val reader = new CTFTraceReader(CTF_trace)

  var input_size = 0

  // default: monotinic clock
  val clock = CTF_trace.getClock
  println(clock.getName)

  while (reader.hasMoreEvents && (size == -1 || input_size <= size)) {
    val event = reader.getCurrentEventDef
    val timestamp = event.getTimestamp
    val cpu_id: Int = event.getCPU
    val fields = event.getFields

    println
    println(timestamp)
    println(s"cpu = ${cpu_id}")
    println("event name: " + event.getDeclaration.getName)
    println(fields)
    for (name <- fields.getFieldNames) {
      val definition: Definition = fields.getDefinition(name)
      val declaration = definition.getDeclaration
      definition match {
        case i: IntegerDefinition => println(name + ": Int = " + i.getValue)
        case f: FloatDefinition   => println(name + ": Double = " + f.getValue)
        case s: StringDefinition  => println(name + ": String = " + s.getValue)
        case _                    => println(name + ": ? = " + definition)
      }
    }

    input_size += 1
    reader.advance
  }

  reader.close

}