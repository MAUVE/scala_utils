package mauve.utils.ctf

import scala.collection.JavaConversions._

object TestTrace extends App {
  val trace = new Trace
  trace.load("/home/ddoose/save/lttng-traces/auto-20171117-153140/ust/uid/1000/64-bit")

  println("Trace size = " + trace.events.size)

  println("\nEvents:")
  val events = trace.events.map(_.getDeclaration.getName).toSet
  for (name <- events) {
    println(name)
  }

  println("\nFields:")
  val fields = trace.events.flatMap(_.getFields.getFieldNames).toSet
  for (name <- fields) {
    println(name)
  }
}